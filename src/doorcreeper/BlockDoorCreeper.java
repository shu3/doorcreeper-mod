package doorcreeper;

import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import net.minecraft.block.BlockDoor;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.IconFlipped;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.monster.EntitySilverfish;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.util.Icon;
import net.minecraft.world.World;

public class BlockDoorCreeper extends BlockDoor {

	protected BlockDoorCreeper(int par1, Material par2Material) {
		super(par1, par2Material);
	}
	
	
	
	@Override
	public boolean onBlockActivated(World par1World, int par2, int par3,
			int par4, EntityPlayer par5EntityPlayer, int par6, float par7,
			float par8, float par9) {
		boolean res = super.onBlockActivated(par1World, par2, par3, par4, par5EntityPlayer,
				par6, par7, par8, par9);
        if (isDoorOpen(par1World, par2, par3, par4)) {
            spawnCreeper(par1World, par2, par3, par4);
		}
		return res;
	}
	
	@Override
	public void onPoweredBlockChange(World par1World, int par2, int par3,
			int par4, boolean par5) {
		boolean isOpened = isDoorOpen(par1World, par2, par3, par4);
		super.onPoweredBlockChange(par1World, par2, par3, par4, par5);
        if (!isOpened &&
        		isDoorOpen(par1World, par2, par3, par4)) {
            spawnCreeper(par1World, par2, par3, par4);
		}
	}
	
	@Override
	public int idPicked(World par1World, int par2, int par3, int par4) {
		return ModDoorCreeper.doorCreeperItem.itemID;
	}
	
	@Override
	public int idDropped(int par1, Random par2Random, int par3) {
		return (par1 & 8) != 0 ? 0 : ModDoorCreeper.doorCreeperItem.itemID;
	}

	private void spawnCreeper(World par1World, int par2, int par3, int par4) {
        if (!par1World.isRemote) {
        	EntityCreeper entitycreeper = new EntityCreeper(par1World);
        	entitycreeper.setLocationAndAngles((double)par2 + 0.5D, (double)par3, (double)par4 + 0.5D, 0.0F, 0.0F);
        	par1World.spawnEntityInWorld(entitycreeper);
        }
	}
}
