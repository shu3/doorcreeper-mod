package doorcreeper;

import net.minecraft.block.Block;
import net.minecraft.block.BlockDoor;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.MinecraftForge;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Init;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.Mod.PostInit;
import cpw.mods.fml.common.Mod.PreInit;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

@Mod(modid="DoorCreeper", name="DoorCreeper", version="0.0.1")
@NetworkMod(clientSideRequired=true, serverSideRequired=false, versionBounds="1.6.2")
public class ModDoorCreeper {
    @Instance("DoorCreeper")
    public static ModDoorCreeper instance;

    public static int doorCreeperID;
    public static int doorCreeperBlockID;
    public static Item doorCreeperItem;
    public static Block doorCreeperBlock;
    public static final String DCModId = "DoorCreeper";

    @SidedProxy(clientSide="doorcreeper.client.ClientProxy", serverSide="doorcreeper.CommonProxy")
    public static CommonProxy proxy;
   
    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
    	try {
            Configuration config = new Configuration(event.getSuggestedConfigurationFile());
            config.load();
            this.doorCreeperID = config.getItem("ItemDoorCreeper", 5002).getInt();
            this.doorCreeperBlockID = config.getBlock("BlockDoorCreeper", 2500).getInt();

            config.save();
    	} catch (ExceptionInInitializerError e) {
			System.out.println(e.getCause().getMessage());
			e.getCause().printStackTrace();
			throw e;
		}
    }
   
    @EventHandler
    public void load(FMLInitializationEvent event) {
        this.doorCreeperItem = new ItemDoorCreeper(this.doorCreeperID, Material.wood);
        this.doorCreeperBlock = new BlockDoorCreeper(this.doorCreeperBlockID, Material.wood);
        this.doorCreeperBlock.setHardness(3.0F).setStepSound(Block.soundWoodFootstep).setUnlocalizedName("DoorCreeper").func_111022_d(this.DCModId + ":door_creeper");
        LanguageRegistry.addName(this.doorCreeperItem, "DoorCreeper");
        LanguageRegistry.instance().addNameForObject(this.doorCreeperItem, "ja_JP", "クリーパーのドア");
		GameRegistry.addRecipe(new ItemStack(this.doorCreeperItem, 1),
				"XX ", "YY ", "YY ",
				'X', Item.slimeBall,
				'Y', Block.planks);

        proxy.registerRenderers();
    }
   
    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
            // Stub Method
    }
}